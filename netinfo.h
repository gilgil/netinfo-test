#pragma once

#include "mac.h"
#include "ip.h"

struct NetInfo {
    bool init(std::string intfName);
    Mac mac_;
    Ip ip_;
};
