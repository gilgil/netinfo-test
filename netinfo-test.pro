TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        ip.cpp \
        mac.cpp \
        main.cpp \
        netinfo.cpp

HEADERS += \
    ip.h \
    mac.h \
    netinfo.h
